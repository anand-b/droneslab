+++
title = "Home"

[[rows]]
  blocks = ["banner"]

[[rows]]
  blocks = [
    "about", 
    "gallery",
  ]
+++
