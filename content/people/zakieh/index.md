+++
weight = 2

title = "Zakieh Hashemifar"
position = "Ph.D. Student"
interests = [
  'Robotics', 'WiFi' , 'SLAM'
]
img = ""
email = "zakiehsa@buffalo.edu"

[links]
  cv = 'CV_Zakieh_Hashemifar.pdf'
  github = ''
  linkedin = 'zakieh'
+++


I was born in Iran in 1988 and received my B.Sc. in Information Technology
Engineering from Tehran University in 2010, and M.Sc degree from Sharif
University of Technology in 2012.  I started my Ph.D. program at University at
Buffalo in department of Computer Science and Engineering in 2014.  Since then
I've been working in the area of localization and mapping for robots under
supervision of professor Karthik Dantu.  I was awarded Graduate Dean's Scholars
Award from Liesl Folks, Dean of University at Buffalo, School of Engineering and
Applied Sciences in 2014.
