+++
weight = 3

title = "Daniel Taillie"
position = "Master's Student"
interests = ["Robotics","Machine Learning","Electrical Engineering"]
img = "profile.jpg"
email = "dptailli@buffalo.edu"

[links]
  linkedin = "daniel-taillie-3a26a8106"
  cv = "Daniel.docx"
+++

Daniel is from Rochester, New York and is 23 years old. He has received a B.A.
in Cognitive Science (2015) and a B.S. in Electrical Engineering (2016) with
minors in Mathematics and Physics from the State University of New York at
Buffalo. He is now pursuing a M.S. in Coputer Science and Engineering at SUNY
Buffalo. His interests are Artificial Intelligence, Robotics, and Machine
Learning.
