+++
weight = 2

title = "Yuyang Chen"
position = "Ph.D. Student"
interests = [
  'Robotics', 'WiFi', 'Multi-Robot Coordination', 'SLAM'
]

email = "yuyangch@buffalo.edu"
img = "profile.jpg"
[links]
  cv = ''
  github = ''
  linkedin = 'yuyang-chen-76738633'
+++

Yuyang Chen was born in GuangXi, China in 1991. He received his B.E. in
Electrical Engineering from State University of New York at Stony Brook in 2014,
and M.Sc. in Electical Engineering from State University fo New York at Buffalo
in 2016. He started his Ph.D. program in Computer Science and Engineering in
June 2016. Since then he has been working under the supervision of professor
Karthik Dantu, in the area of Robotics, Micro-Aerial Vehcles and Artificial
Intelligence.
