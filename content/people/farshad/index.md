+++
weight = 2

title = "Farshad Ghanei"
position = "Ph.D. Student"
interests = [
  'Embedded Systems'
]

email = "farshadg@buffalo.edu"
img = "profile.jpg"
[links]
  cv = 'CV_Farshad_Ghanei.pdf'
  github = ''
  linkedin = 'farshad-ghanei'
+++

Farshad Ghanei was born in Tehran, Iran in 1989. He received his B.Sc. in
Electrical Engineering from Sharif University of Technology in 2013, and
M.Sc degree from State University of New York at Buffalo  in 2015. While
studying at University at Buffalo, he started his Ph.D. program in department of
Computer Science and Engineering in 2014. Since then he has been working under
supervision of professor Karthik Dantu, in the area of operating systems and
embedded systems.  He was awarded Graduate Dean's Scholars Award from
Liesl Folks, Dean of University at Buffalo, School of Engineering and
Applied Sciences in 2013.


