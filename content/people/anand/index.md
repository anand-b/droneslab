+++
weight = 4

title = "Anand Balakrishnan"
position = "Undergraduate Student"
interests = [
  'Robotics',
  'Embedded Systems',
  'Artificial Intelligence',
]

email = "anandbal@buffalo.edu"
img = "profile.jpg"
[links]
  github = "anand-bala"
  linkedin = "anandb1597"
  cv = "AnandBalakrishnanCV.pdf"

+++

I am an Undergraduate Computer Engineering student at the University at Buffalo,
currently in my Senior Year. I work in the [Distributed Robotics and Networked
Embedded Systems Lab](http://drones.cse.buffalo.edu) at the
University at Buffalo directed by [Karthik
Dantu](https://www.cse.buffalo.edu//faculty/kdantu/). I work on projects
involving WiFi Sensing, Simultaneous Localization and Mapping, and Swarm
Robotics.

## Research Interests

I am interested in the development of autonomous robot systems. For this, I want
to research on novel sensing methods, long-term autonomy, multi-robot
coordination, and human-robot interaction. 

## Publications 
{{< pubs bib="pubs.bib" author="" >}}
