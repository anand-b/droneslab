+++
weight = 1

title = "Karthik Dantu"
position = "Faculty"
email = "kdantu@buffalo.edu"
img = "profile.jpg"
interests = ["Robotics","Embedded systems","Sensor Systems"]

[links]
  linkedin = "karthik-dantu-9b83051"
+++

## About Me

I am an Assistant Professor in Computer Science and Engineering at University at
Buffalo. My research interests are in multi-robot systems, embedded systems, and
mobile systems. In particular, I am interested in sensing and coordination
challenges in such systems.

Outside of research, I'm obsessed with sports (football and cricket), coffee and
trying esoteric diets.


