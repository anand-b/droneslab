+++
weight = 2

title = "Charuvahan Adhivarahan"
position = "Ph.D. Student"
interests = [
  'Robotics', 'WiFi', 'Multi-Robot Coordination', 'SLAM'
]

email = "charuvah@buffalo.edu"
img = "profile.jpg"
[links]
  cv = ''
  github = ''
  linkedin = ''
+++

Charuvahan was born in Neyveli, India in 1988. He received his B.E. in
Computer Science and Engineering from Annamalai University, Chidambaram, Tamil Nadu
in the year 2009. He is a Ph.D. student at the University at Buffalo, The State University
of New York under the supervision of Professor Karthik Dantu in the area of Robotics.
