+++
title = "Wi-Fi/RGB-D Dataset"
img = 'maps/bell.jpg'
authors = [
'Anand Balakrishnan',
'Nicholas Ceccarelli',
'Harshdeep Sokhey',
'Charuvahan Adhivarahan',
'Zakieh Hashemifar',
'Karthik Dantu',
]
maintainers = [
'Anand Balakrishnan',
'Nicholas Ceccarelli',
'Harshdeep Sokhey'
]

[[links]]
  url = '//drones.cse.buffalo.edu/dataset/RGBD-W/'
  title = 'Dataset Files'

[[links]]
  url = '//github.com/anand-bala/wifi-dataset'
  title = 'Dataset ROS Workspaces'

[[links]]
  url = '#'
  title = 'Raw Dataset SDK'

[[links]]
  url = '#'
  title = 'Groundtruth Evaluation Tools'
+++


This project aims to compile the **first RGB-D dataset that combines Wi-Fi
information** to develop _Wi-Fi augmented sensing algorithms_. This dataset can
be of  particular value for development of visual sensing algorithms for mapping
and navigating within buildings equipped with Wi-Fi access points. The goal of
the dataset is to support the development of sensing algorithms with improved
accuracy and efficiency using information provided by Wi-Fi signals.
<!--more-->

## Overview

The almost ubiquitous nature of Wi-Fi in indoor environments, it can be
harnessed as a valuable tool in enhancing the efficiency of sensing, especially
in simultaneous localization and mapping (SLAM). Recent trends in sensing have
seen the use of regular and depth cameras together (with sensors such as
Microsoft Kinect) for 3D mapping.[^rgbd-slam] However, algorithms reasoning with
RGB-D sensors come with challenges when performing SLAM indoors, including:

* __Perceptual Aliasing__:

    > Indoor environments tend to have architectures that are symmetric and
    > repetitive. Corridors with bland walls and repeated patterns with doors
    > and lights could potentially cause the SLAM algorithm to get confused
    > between the places (also called wrong loop closure) resulting in faulty
    > maps and bad localization. Further, if the robot/mobile device loses
    > position information (kidnapped robot), it might be extremely challenging
    > to re-localize.

* __Computation Time__:

    > Cameras usually produce huge amount of data. For example, the Kinect has
    > a frame rate of 30 fps and each frame has more than 300000 points
    > including color and depth data. Extracting and matching features between
    > depth/RGB images are computationally intensive tasks. These comparisons
    > need to be constantly made to estimate visual transformations and detect
    > loops for correcting errors in the constructed map. In a graph-based SLAM
    > algorithm (such as RGBD SLAM), the number of images for potential
    > comparisons increase with time making it harder to run them online.

These problems can be mitigated by use of additional, less expensive information
sources like Wi-Fi. For example, Wi-Fi can be used to compute coarse
localization, thereby reducing the number of images for potential comparison.

[^rgbd-slam]: These includes algorithms like [RGBD SLAM](https://github.com/felixendres/rgbdslam_v2), [RTAB-Map](http://introlab.github.io/rtabmap/), and [ORB-SLAM](https://webdiis.unizar.es/~raulmur/orbslam/)

## The Robot

The dataset was collected using a Turtlebot 2, which is controlled
by a laptop notebook placed on one of its shelves using ROS packages for
driving the Turtlebot.[^turtlebot_ros]. The laptop also logs the following data
in _rosbag_[^rosbag] binary format:


* _Proprioceptive Data_ collected from sensors in the Kobuki base, namely:
    * Gyroscope.
    * Wheel-encoder odometry.
* _Perceptual Data_ collected from:
    * _Velodyne Puck&trade; (VLP-16) 3D LiDAR_. The data is in the form of
        a stream of 3D pointclouds compatible with the Pointcloud Library (PCL)
        and ROS.
    * _Microsoft Kinect&trade; RGB-D Camera_. The data is collected as a stream of
        RGB-D images, and stored as 2 images:
        * Depth Intensity: A PNG file containing the depth intensity of the image.
        * Raw Image: A PNG file with the image obtained from the Kinect.
* _Wi-Fi Signal Information_ collected from onboard the laptop. The Wi-Fi data
    consists of the _BSSID_ of every access point detected by the card at a given
    timestamp, along with the signal strength of the access point.


The laptop also logs the coordinate frames and the transforms between the frames
of each sensor on the robot through the [tf](https://wiki.ros.org/tf) package
provided by ROS. The package outputs the transforms between the various frames
defined in the robot in the form of a tree, that can be visualized using tools
like [rqt_graph](https://wiki.ros.org/rqt_graph) and
[tf2_tools](https://wiki.ros.org/tf2_tools).




[^turtlebot]: https://www.clearpathrobotics.com/turtlebot-2-open-source-robot/
[ros]: http://www.ros.org
[^turtlebot_ros]: http://wiki.ros.org/turtlebot
[^rosbag]: https://wiki.ros.org/rosbag/

