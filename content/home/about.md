+++
title = "About"

widget = "card"
facls = "fas fa-users"
width = "md-6"
weight = 1
+++

The **D**istributed **Ro**botics and **N**etworked **E**mbedded **S**ystems
(DRONES) Lab is part of the [Department of Computer Science and
Engineering](https://engineering.buffalo.edu/computer-science-engineering.html)
at the [University at Buffalo](https://www.buffalo.edu).

At the DRONES Lab, we broadly work on robotics, embedded systems, and mobile
computing. You can check out our ongoing and past projects in the [Research]({{<
secpath "research/_index.md" >}}) tab, and the list of publications in the
[Publications]({{< secpath "publications/_index.md" >}}) tab.



