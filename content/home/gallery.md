+++
title = "Gallery"
widget = "carousel"

[[carousel]]
  img = "/img/overlay/davis2.png"
[[carousel]]
  img = "/img/overlay/davis3.jpeg"
[[carousel]]
  img = "/img/overlay/davis4.jpeg"
[[carousel]]
  img = "/img/overlay/davis5.jpg"
+++
